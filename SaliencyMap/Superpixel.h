#ifndef SUPERPIXEL_H
#define SUPERPIXEL_H

#define NOMINMAX

#include <iostream>
#include <string>
#include "Bin.h"
#include "GlobalHistogram.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

class Image;

class Superpixel {
	public:
		vector<double> colorSim_;
		vector<double> distanceSim_;
		vector<double> sim_;
		vector<double> weight_;
		double saliency_;

		double colorContrast_;
		double depthContrast_;
		double spatialSpread_;

		Superpixel(Mat, Image*);

		inline vector<Bin>& getHist(){ return hist_; };
		inline Mat* getMask() { return &mask_; };
		inline int getSize() const { return size_; };

		inline const Point& getCenter() const { return center_; };
		inline const color_t& getMeanColor() { return meanColor_; };
		inline const double getMeanDepth() { return meanDepth_; };

	private:
		Image* im_;

		vector<Bin> hist_;
		Mat mask_;
		int size_;

		Point center_;
		color_t meanColor_;
		double meanDepth_;

		void createHistogram();
		int getClosestBin(vector<Bin>&, Vec3b);
		void normalizeHist();
		void computeCenter();
};

#endif