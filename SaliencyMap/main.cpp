#include <stdio.h>
#include "Image.h"
#include "SaliencyMap.h"
#include "Comparison.h"

using namespace cv;
using namespace std;

const int supNumber = 300;

#define SAFE_DELETE(pPtr) { delete pPtr; pPtr = NULL; }

int main(int argc, char* argv[])
{
	string datasetUrl = argv[1], file = argv[2];

	Image *image = new Image(datasetUrl, file, supNumber);
	SaliencyMap *SM = new SaliencyMap(image);

	//image->printSegmentedImage();

	//imshow("saliency map", *SM->getSaliencyMap());
	//waitKey(0);

	Comparison *comp = new Comparison(*SM->getSaliencyMap(), datasetUrl, file);
	comp->computeROC();
	comp->computeNSS();

	SAFE_DELETE(image);
	SAFE_DELETE(SM);
	SAFE_DELETE(comp);
}