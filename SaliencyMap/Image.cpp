#include "Image.h"

const string rgbDir = "RGB/";
const string depthDir = "depth/";

#define SAFE_DELETE(pPtr) { delete pPtr; pPtr = NULL; }

void loadImageSLIC(const Mat& inimg, gSLICr::UChar4Image* outimg)
{
	gSLICr::Vector4u* outimg_ptr = outimg->GetData(MEMORYDEVICE_CPU);

	for (int y = 0; y < outimg->noDims.y; y++)
		for (int x = 0; x < outimg->noDims.x; x++)
		{
			int idx = x + y * outimg->noDims.x;
			outimg_ptr[idx].b = inimg.at<Vec3b>(y, x)[0];
			outimg_ptr[idx].g = inimg.at<Vec3b>(y, x)[1];
			outimg_ptr[idx].r = inimg.at<Vec3b>(y, x)[2];
		}
}

void loadImageSLIC(const gSLICr::UChar4Image* inimg, Mat& outimg)
{
	const gSLICr::Vector4u* inimg_ptr = inimg->GetData(MEMORYDEVICE_CPU);

	for (int y = 0; y < inimg->noDims.y; y++)
		for (int x = 0; x < inimg->noDims.x; x++)
		{
			int idx = x + y * inimg->noDims.x;
			outimg.at<Vec3b>(y, x)[0] = inimg_ptr[idx].b;
			outimg.at<Vec3b>(y, x)[1] = inimg_ptr[idx].g;
			outimg.at<Vec3b>(y, x)[2] = inimg_ptr[idx].r;
		}
}

Image::Image(const string &datasetUrl, const string &file, int supCount)
	: supNum_(0)
{
	string rgbImgPath = datasetUrl + rgbDir + file + ".jpg";
	originalImage_ = imread(rgbImgPath, CV_LOAD_IMAGE_COLOR);
	cvtColor(originalImage_, labImage_, CV_BGR2Lab);

	string depthImgPath = datasetUrl + depthDir + file + ".png";
	depthImage_ = imread(depthImgPath, CV_LOAD_IMAGE_GRAYSCALE);

	height_ = originalImage_.rows;
	width_ = originalImage_.cols;
	diagonal_ = sqrt(double(pow(width_, 2.0) + pow(height_, 2.0)));

	// gSLICr settings
	mySettings_.img_size.x = originalImage_.cols;
	mySettings_.img_size.y = originalImage_.rows;
	mySettings_.no_segs = supCount;
	mySettings_.spixel_size = 30;
	mySettings_.coh_weight = 2.6f;
	mySettings_.no_iters = 5;
	mySettings_.color_space = gSLICr::RGB; // gSLICr::CIELAB for Lab, or gSLICr::RGB for RGB
	mySettings_.seg_method = gSLICr::GIVEN_NUM; // or gSLICr::GIVEN_NUM for given number
	mySettings_.do_enforce_connectivity = true; // wheter or not run the enforce connectivity step

	// instantiate a core_engine
	gSLICrEngine_ = new gSLICr::engines::core_engine(mySettings_);

	// gSLICr takes gSLICr::UChar4Image as input
	gSLICr::UChar4Image* inImg = new gSLICr::UChar4Image(mySettings_.img_size, true, true);

	loadImageSLIC(originalImage_, inImg);

	gSLICrEngine_->Process_Frame(inImg);

	gHist_ = new GlobalHistogram(this);

	createSuperpixelImage();

	SAFE_DELETE(inImg);
};

Image::~Image()
{
	SAFE_DELETE(gHist_);
	SAFE_DELETE(gSLICrEngine_);
}

void Image::createSuperpixelImage()
{
	const gSLICr::IntImage* idxImg = gSLICrEngine_->Get_Seg_Res();
	const int* dataPtr = idxImg->GetData(MEMORYDEVICE_CPU);

	Mat superpixelImage(originalImage_.size(), CV_8U);

	for (int j = 0; j < height_; ++j)
	{
		for (int i = 0; i < width_; ++i)
		{
			int xx = j * width_ + i;
			ushort label = (ushort)dataPtr[xx];
			if (label > supNum_)
				supNum_ = label;
			superpixelImage.at<uchar>(j, i) = label;
		}
	}

	Mat supMask;
	supNum_++;
	for (int i = 0; i < supNum_; i++)
	{
		inRange(superpixelImage, i, i, supMask);

		//imshow("sup", supMask);
		//waitKey(0);
		if (countNonZero(supMask) == 0)
			continue;

		superpixels.push_back(Superpixel(supMask, this));
	}

};

void Image::printSegmentedImage()
{
	gSLICr::UChar4Image* outImg = new gSLICr::UChar4Image(mySettings_.img_size, true, true);

	Size s(mySettings_.img_size.x, mySettings_.img_size.y);
	Mat boundryDrawFrame; boundryDrawFrame.create(s, CV_8UC3);

	gSLICrEngine_->Draw_Segmentation_Result(outImg);

	loadImageSLIC(outImg, boundryDrawFrame);
	imshow("segmentation", boundryDrawFrame);
	waitKey(0);

	SAFE_DELETE(outImg);
}