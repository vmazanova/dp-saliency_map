#ifndef COMPARISON_H
#define COMPARISON_H
#include <fstream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <string>

using namespace std;
using namespace cv;

class Comparison {
	public:
		Comparison(Mat &, const string &, const string &);

		void computePrecisonRecall();
		void computeROC();
		void computeNSS();

	private:
		Mat groundTruth_;
		Mat saliencyMap_;

		string datasetUrl_;
		string filename_;
};

#endif