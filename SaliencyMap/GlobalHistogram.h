#ifndef GLOBALHISTOGRAM_H
#define GLOBALHISTOGRAM_H

#define NOMINMAX

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "Bin.h"

using namespace cv;
using namespace std;

class Image;

class GlobalHistogram {
	public:
		GlobalHistogram(Image *);

		inline vector<Bin>& getHist(){ return hist_; };
		inline int getSize(){ return size_; };

	private:
		vector<Bin> hist_;
		int size_;
		Image *im_;

		void createBins(std::vector<int*>*);
		void computeBinMeanColor(vector<int*>*);
		int getMergeStartPosition();
		double getBinColorDistance(const Bin &, const Bin &);
		void mergeBins();

};

#endif;