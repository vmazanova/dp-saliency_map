﻿#include "SaliencyMap.h"

const int kMaxPixelValue = 255;

template <typename Type>
void assignMinMax(Type value, Type *min, Type *max)
{
	if (value > *max)
		*max = value;

	if (value < *min)
		*min = value;
}

SaliencyMap::SaliencyMap(Image *im)
	: superpixels_(im_->getSuperpixels()),
	  im_(im)
{
	computeSpatialSupSimilarity();

	computeColorSupSimilarity();

	double sim;
	int sup2Idx;

	//global similarity of superpixels
	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();
			sim = sup1->colorSim_[sup2Idx] * sup1->distanceSim_[sup2Idx];
			sup1->sim_[sup2Idx] = sim;
		}
	}

	computeSupWeight();

	computeDepthContrast();

	computeColorContrast();

	computeSpatialSpread();

	computeSaliencyValues();

	generateSaliencyMap();
}

void SaliencyMap::computeSpatialSupSimilarity()
{
	double diffX, diffY, distance, similarity;
	int sup2Idx;

	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		//histogram intersection
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();

			//compute euclidean distance between centers of superpixels
			diffX = sup1->getCenter().x - sup2->getCenter().x;
			diffY = sup1->getCenter().y - sup2->getCenter().y;

			distance = sqrt(double(pow(diffX, 2) + pow(diffY, 2)));

			similarity = 1 - (distance / im_->getDiagonal());
			sup1->distanceSim_[sup2Idx] = similarity;
		}
	}
}

void SaliencyMap::computeColorSupSimilarity()
{
	double colorSim;
	int sup2Idx;

	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		//histogram intersection
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();
			colorSim = 0;

			for (int i = 0; i < im_->getHistogram()->getSize(); i++)
				colorSim += MIN(sup1->getHist()[i].size, sup2->getHist()[i].size);

			sup1->colorSim_[sup2Idx] = colorSim;
		}
	}
}

void SaliencyMap::computeSupWeight()
{
	double weight;
	int sup2Idx;

	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();
			weight = sup2->getSize() * sup1->distanceSim_[sup2Idx];
			sup1->weight_[sup2Idx] = weight;
		}
	}
}

void SaliencyMap::computeDepthContrast()
{
	double minDC(std::numeric_limits<double>::max()), maxDC(0);
	double *depthContrast, distance;
	int sup1Idx, sup2Idx;

	depthContrast = new double[superpixels_->size()];
	memset(depthContrast, 0, sizeof(double) * superpixels_->size());

	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		sup1Idx = sup1 - superpixels_->begin();
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();

			if (sup1Idx == sup2Idx)
				continue;

			distance = std::abs(sup1->getMeanDepth() - sup2->getMeanDepth());

			depthContrast[sup1Idx] += sup1->weight_[sup2Idx] * distance;

			//find the minimum and maximum superpixel contrast
			assignMinMax(depthContrast[sup1Idx], &minDC, &maxDC);
		}
	}

	//normalization of depth contrast
	for (auto sup = superpixels_->begin(); sup != superpixels_->end(); ++sup)
	{
		sup1Idx = sup - superpixels_->begin();
		sup->depthContrast_ = (depthContrast[sup1Idx] - minDC) / (maxDC - minDC);
	}

	delete[] depthContrast;
}

//compute color contrast of each superpixel
void SaliencyMap::computeColorContrast()
{
	double minGC(std::numeric_limits<double>::max()), maxGC(0);
	double *colorContrast, distance, diff;
	int sup1Idx, sup2Idx;

	colorContrast = new double[superpixels_->size()];
	memset(colorContrast, 0, sizeof(double) * superpixels_->size());

	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		sup1Idx = sup1 - superpixels_->begin();
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();

			if (sup1Idx == sup2Idx)
				continue;

			//euclidean distance of superpixels mean color
			diff = 0;
			for (int i = 0; i < kNumColor; i++)
				diff += pow(sup1->getMeanColor()[i] - sup2->getMeanColor()[i], 2);

			distance = sqrt(double(diff));

			//formula for the color contrast
			colorContrast[sup1Idx] += sup1->weight_[sup2Idx] * distance;

			//find the minimum and maximum superpixel contrast
			assignMinMax(colorContrast[sup1Idx], &minGC, &maxGC);
		}
	}

	//normalization of color contrast
	for (auto sup = superpixels_->begin(); sup != superpixels_->end(); ++sup)
	{
		sup1Idx = sup - superpixels_->begin();
		sup->colorContrast_ = (colorContrast[sup1Idx] - minGC) / (maxGC - minGC);
	}

	delete[] colorContrast;
}

void SaliencyMap::computeSpatialSpread()
{
	double maxSS(0), minSS(std::numeric_limits<double>::max());
	double *spatialSpread, diffX, diffY, distance, fracUp, fracDown;
	int sup1Idx, sup2Idx;

	spatialSpread = new double[superpixels_->size()];
	memset(spatialSpread, 0, sizeof(double) * superpixels_->size());

	//compute spatial spread of each superpixel
	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		fracUp = fracDown = 0;
		sup1Idx = sup1 - superpixels_->begin();
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();

			if (sup1Idx == sup2Idx)
				continue;

			diffX = sup1->getCenter().x - sup2->getCenter().x;
			diffY = sup1->getCenter().y - sup2->getCenter().y;

			distance = sqrt(double(pow(diffX, 2) + pow(diffY, 2)));

			fracUp += sup1->sim_[sup2Idx] * distance;
			fracDown += sup1->sim_[sup2Idx];
		}

		spatialSpread[sup1Idx] = fracUp / fracDown;

		//maximum and minimum of spatial spread
		assignMinMax(spatialSpread[sup1Idx], &minSS, &maxSS);
	}

	//normalize spatial spread
	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		sup1Idx = sup1 - superpixels_->begin();
		sup1->spatialSpread_ = (spatialSpread[sup1Idx] - maxSS) / (minSS - maxSS);
	}

	delete[] spatialSpread;
}

void SaliencyMap::computeSaliencyValues()
{
	double rccFracUp, rdcFracUp, rssFracUp, fracDown, rss, rcc, rdc;
	int sup1Idx, sup2Idx;

	//refine superpixel values and compute saliency value for each superpixel
	for (auto sup1 = superpixels_->begin(); sup1 != superpixels_->end(); ++sup1)
	{
		rccFracUp = rdcFracUp = fracDown = rssFracUp = 0;
		sup1Idx = sup1 - superpixels_->begin();
		for (auto sup2 = superpixels_->begin(); sup2 != superpixels_->end(); ++sup2)
		{
			sup2Idx = sup2 - superpixels_->begin();

			if (sup1Idx == sup2Idx)
				continue;

			rccFracUp += sup1->sim_[sup2Idx] * sup2->colorContrast_;
			rdcFracUp += sup1->sim_[sup2Idx] * sup2->depthContrast_;
			rssFracUp += sup1->sim_[sup2Idx] * sup2->spatialSpread_;
			fracDown += sup1->sim_[sup2Idx];
		}

		rcc = rccFracUp / fracDown;
		rdc = rdcFracUp / fracDown;
		rss = rssFracUp / fracDown;

		sup1->saliency_ = rcc * rss;
	}
}

void SaliencyMap::generateSaliencyMap()
{
	int maxSal(0), minSal(kMaxPixelValue);
	saliencyMap_ = Mat(im_->getOriginalImage()->rows, im_->getOriginalImage()->cols, CV_8UC1, cvScalar(0.));

	//find minimal and maximal value for the following normalization
	for (auto sup = superpixels_->begin(); sup != superpixels_->end(); ++sup)
	{
		int color = (int)(sup->saliency_ * kMaxPixelValue);

		assignMinMax(color, &minSal, &maxSal);
	}

	for (auto sup = superpixels_->begin(); sup != superpixels_->end(); ++sup)
	{
		int color = (int)(sup->saliency_ * kMaxPixelValue);
		color = ((color - minSal) * kMaxPixelValue) / (maxSal - minSal);

		saliencyMap_.setTo(color, *sup->getMask());
	}

	advancedWeightedDepth();
}

void SaliencyMap::simpleWeightedDepth()
{
	Mat depth = *im_->getDepthImage();

	Mat sum(depth.rows, depth.cols, CV_32FC1);
	Mat depthF, saliencyF;

	depth.convertTo(depthF, CV_32FC1);
	saliencyMap_.convertTo(saliencyF, CV_32FC1);

	divide(saliencyF, depthF, sum);
	normalize(sum, saliencyMap_, 0, 255, NORM_MINMAX, CV_8UC1);
}

void SaliencyMap::advancedWeightedDepth()
{
	Mat depth = *im_->getDepthImage();

	Mat depthF, saliencyF;
	depth.convertTo(depthF, CV_32FC1);
	saliencyMap_.convertTo(saliencyF, CV_32FC1);

	int y, x;
	float *saliencyP, *depthP;
	for (y = 0; y < saliencyF.rows; ++y)
	{
		saliencyP = saliencyF.ptr<float>(y);
		depthP = depthF.ptr<float>(y);
		for (x = 0; x < saliencyF.cols; ++x)
		{
			int pixDepth = depthP[x];

			float weight;
			if (pixDepth < 30)
				weight = 0;
			else
				weight = 2.5 * pow(10, -7) * pow(pixDepth, 3) - 0.00017 * pow(pixDepth, 2) + 0.03 * pixDepth - 0.75;

			saliencyF.ptr<float>(y)[x] *= weight;
		}
	}

	normalize(saliencyF, saliencyMap_, 0, 255, NORM_MINMAX, CV_8UC1);
}