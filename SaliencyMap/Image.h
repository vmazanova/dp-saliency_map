#ifndef IMAGE_H
#define IMAGE_H

#include <iostream>
#include <string>
#include "gSLICr_Lib/gSLICr.h"
#include "Superpixel.h"
#include "GlobalHistogram.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;


class Image {
	public:
		Image(const string &, const string &, int);
		~Image();

		inline int getHeight(){ return height_; };
		inline int getWidth(){ return width_; };
		inline double getDiagonal(){ return diagonal_; };

		inline int getSupNum() { return supNum_; };
		inline GlobalHistogram* getHistogram(){ return gHist_; };
		inline Mat* getOriginalImage(){ return &originalImage_; };
		inline Mat* getLabImage(){ return &labImage_; };
		inline Mat* getDepthImage(){ return &depthImage_; };
		inline vector<Superpixel>* getSuperpixels() { return &superpixels; };

		void printSegmentedImage();

	private:
		gSLICr::objects::settings mySettings_;
		gSLICr::engines::core_engine* gSLICrEngine_;

		GlobalHistogram *gHist_;

		int supNum_;
		int width_;
		int height_;
		double diagonal_;

		Mat originalImage_;
		Mat labImage_;
		Mat depthImage_;

		vector<Superpixel> superpixels;

		void createSuperpixelImage();
};

#endif