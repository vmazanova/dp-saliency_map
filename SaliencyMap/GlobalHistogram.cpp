#include "GlobalHistogram.h"
#include "Image.h"

const int kMaxSize = 16*16*16;
const double kPixelsPercentage = 0.95;

#define SAFE_DELETE(pPtr) { delete[] pPtr; pPtr = NULL; }

void myDelete(int *p)
{
	delete p;
	p = NULL;
}

GlobalHistogram::GlobalHistogram(Image *im)
	: size_(0),
	im_(im)
{
	hist_.resize(kMaxSize);
	vector<int*> bins[kMaxSize];

	createBins(bins);

	//create a mean color for each bin and insert it to the glob_hist
	computeBinMeanColor(bins);

	//sort bins in histogram by their number of pixels
	std::sort(hist_.begin(), hist_.end());

	//merge remaining bins
	mergeBins();

	for (int i = 0; i < kMaxSize; i++)
		std::for_each(bins[i].begin(), bins[i].end(), myDelete);
}

//TO DO : REVIEW
void GlobalHistogram::createBins(vector<int*> bins[kMaxSize])
{
	Mat labImage = *im_->getLabImage();

	//check each pixel in the image
	for (int y = 0; y < labImage.rows; y++)
	{
		for (int x = 0; x < labImage.cols; x++)
		{
			//get a color from the pixel
			Vec3b bgrPixel = labImage.at<Vec3b>(y, x);

			//push it to the vector
			int *tmp = new int[kNumColor];
			for (int k = 0; k < kNumColor; k++)
				*(tmp + k) = bgrPixel.val[k];

			int index = (bgrPixel.val[0] / 16 * 16 * 16 + bgrPixel.val[1] / 16 * 16 + bgrPixel.val[2] / 16);

			//add a color to bin according to the index
			bins[index].push_back(tmp);
		}
	}
}

void GlobalHistogram::computeBinMeanColor(vector<int*> bins[kMaxSize])
{
	//loop through all the bins in histogram
	for (int i = 0; i < kMaxSize; i++)
	{
		int binSize = bins[i].size();

		if (binSize == 0)
			continue;

		//accumulate values from bin
		double accumRgb[kNumColor] = { 0, 0, 0 };
		for (int j = 0; j < binSize; j++)
		{
			int *tmp = bins[i][j];
			for (int k = 0; k < kNumColor; k++)
				accumRgb[k] += tmp[k];
		}

		//assign a mean color to a bin
		for (int k = 0; k < kNumColor; k++)
			hist_[size_].color[k] = accumRgb[k] / binSize;
		hist_[size_].size = (double)binSize;

		size_++;
	}
	size_--;
}

int GlobalHistogram::getMergeStartPosition()
{
	//get a start position from which the merge begins
	double binSum(0);
	int startMergeId(0), imageSize = im_->getHeight() * im_->getWidth();

	while (binSum < kPixelsPercentage * imageSize)
	{
		binSum += hist_[startMergeId].size;
		startMergeId++;
	}

	return startMergeId;
}

double GlobalHistogram::getBinColorDistance(const Bin &bin1, const Bin &bin2)
{
	int accumColor = 0;
	for (int i = 0; i < kNumColor; i++)
	{
		int colorDiff = bin1.color[i] - bin2.color[i];
		accumColor += colorDiff * colorDiff;
	}

	return sqrt(double(accumColor));
}

void GlobalHistogram::mergeBins()
{
	int startMergeId = getMergeStartPosition(), id;
	double minDist, distance;
	for (int i = startMergeId; i < size_; i++)
	{
		minDist = std::numeric_limits<double>::max();
		id = -1;
		for (int j = 0; j < startMergeId; j++)
		{
			distance = getBinColorDistance(hist_[i], hist_[j]);

			if (distance < minDist)
			{
				minDist = distance;
				id = j;
			}
		}

		if (id != -1)
		{
			//bin i joins the bin id
			for (int k = 0; k < kNumColor; k++)
				hist_[id].color[k] = (hist_[id].color[k] + hist_[i].color[k]) / 2;
			hist_[id].size++;
		}
	}

	size_ = startMergeId;
}