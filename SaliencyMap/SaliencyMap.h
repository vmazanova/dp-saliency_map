#ifndef SALIENCYMAP_H
#define SALIENCYMAP_H

#define NOMINMAX

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "Image.h"

using namespace cv;
using namespace std;

class SaliencyMap {
	public:
		SaliencyMap(Image*);
		inline Mat* getSaliencyMap() { return &saliencyMap_; };

	private:
		Image *im_;
		Mat saliencyMap_;
		vector<Superpixel> *superpixels_;

		void simpleWeightedDepth();
		void advancedWeightedDepth();

		void generateSaliencyMap();
		void computeSaliencyValues();
		void computeSpatialSpread();
		void computeColorContrast();
		void computeDepthContrast();
		void computeSupWeight();
		void computeSpatialSupSimilarity();
		void computeColorSupSimilarity();
};

#endif