#ifndef BIN_H
#define BIN_H

static const int kNumColor = 3;

class Bin {
	public:
		Bin() : size(0) {
			memset(color, 0, sizeof(color));
		}

		inline bool operator< (const Bin& rhs){ return size > rhs.size; }
		inline bool operator> (const Bin& rhs){ return *this > rhs; }
		inline bool operator<=(const Bin& rhs){ return !(*this > rhs); }
		inline bool operator>=(const Bin& rhs){ return !(*this < rhs); }

		inline bool operator==(const Bin& rhs){ return size == rhs.size; }
		inline bool operator!=(const Bin& rhs){ return !(*this == rhs); }

		double size;
		int color[kNumColor];
};

typedef double color_t[kNumColor];

#endif