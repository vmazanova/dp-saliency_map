#include "Superpixel.h"
#include "Image.h"

Superpixel::Superpixel(Mat mask, Image *im)
	: im_(im),
	  size_(0)
{
	mask.copyTo(mask_);

	int gHistSize = im_->getHistogram()->getSize();
	hist_.resize(gHistSize);

	memset(meanColor_, 0, sizeof(meanColor_));

	int supNum = im_->getSupNum();
	colorSim_.resize(supNum);
	distanceSim_.resize(supNum);
	sim_ .resize(supNum);
	weight_.resize(supNum);

	Mat invertedDepthImage;
	bitwise_not(*im_->getDepthImage(), invertedDepthImage);
	meanDepth_ = mean(invertedDepthImage, mask_).val[0];

	createHistogram();

	normalizeHist();

	computeCenter();
}

//find Bin from the global histogram with the smallest color distance from a particular Bin
int Superpixel::getClosestBin(vector<Bin>& gHist, Vec3b bgrPixel)
{
	double minDist(std::numeric_limits<double>::max()), distance;
	int gHistSize = im_->getHistogram()->getSize();
	int id = -1, accColor, colorDiff;

	for (int i = 0; i < gHistSize; i++)
	{
		accColor = 0;
		for (int j = 0; j < kNumColor; j++)
		{
			colorDiff = bgrPixel.val[j] - gHist[i].color[j];
			accColor += colorDiff * colorDiff;
		}

		distance = sqrt(double(accColor));

		//find the minimal distance
		if (distance < minDist)
		{
			minDist = distance;
			id = i;
		}
	}

	return id;
}

void Superpixel::createHistogram()
{
	vector<Bin> gHist = im_->getHistogram()->getHist();
	Mat labImage = *im_->getLabImage();

	int y, x;
	Vec3b *labImageP;
	uchar *maskP;
	for (y = 0; y < mask_.rows; ++y)
	{
		labImageP = labImage.ptr<Vec3b>(y);
		maskP = mask_.ptr<uchar>(y);
		for (x = 0; x < mask_.cols; ++x)
		{
			if (maskP[x] == 255)
			{
				Vec3b bgrPixel = labImageP[x];
				for (int k = 0; k < kNumColor; k++)
					meanColor_[k] += bgrPixel.val[k];

				int id = getClosestBin(gHist, bgrPixel);

				if (id != -1)
				{
					//assign color from Bin of global histogram to Bin of superpixel histogram
					for (int k = 0; k < kNumColor; k++)
						hist_[id].color[k] = gHist[id].color[k];

					hist_[id].size++;
				}

				size_++;
			}
		}
	}

	for (int i = 0; i < kNumColor; i++)
		meanColor_[i] /= size_;
}

void Superpixel::normalizeHist()
{
	double accSize(0);
	int gHistSize = im_->getHistogram()->getSize();
	for (int i = 0; i < gHistSize; i++)
		accSize += hist_[i].size;

	for (int i = 0; i < gHistSize; i++)
		if (hist_[i].size != 0)
			hist_[i].size /= accSize;
}

void Superpixel::computeCenter()
{
	Moments m = moments(mask_, true);
	center_ = Point(m.m10 / m.m00, m.m01 / m.m00);
}