#include "Comparison.h"

const string gtDir = "fixation/";
const string saliencyDir = "saliency/";
const string precisionRecallDir = "precision&recall/";
const string rocDir = "roc/";
const string NSSDir = "NSS/";

Comparison::Comparison(Mat &saliencyMap, const string& datasetUrl, const string& filename)
{
	datasetUrl_ = datasetUrl;
	filename_ = filename;

	//load a ground truth image as grayscale
	string gtPath = datasetUrl + gtDir + filename + ".jpg";
	groundTruth_ = imread(gtPath, CV_LOAD_IMAGE_GRAYSCALE);

	double min, max;
	minMaxLoc(saliencyMap, &min, &max);
	saliencyMap.convertTo(saliencyMap_, CV_8UC1, 255.0 / (max - min), -255.0 * min / (max - min));

	string saliencyPath = datasetUrl + saliencyDir + filename + ".jpg";
	imwrite(saliencyPath, saliencyMap_);
}

void Comparison::computePrecisonRecall()
{
	//file to store results
	ofstream myfile;

	//create a path for a file
	string precisionRecallPath = datasetUrl_ + precisionRecallDir + filename_ + ".txt";
	myfile.open(precisionRecallPath, std::ios_base::app);

	int hit(0), sf(0), gf(0);
	float precision, recall;

	//loop through all the thresholds
	for (int thresh = 0; thresh < 255; thresh++)
	{
		int y, x;
		uchar *imageP, *groundTruthP;
		for (y = 0; y < saliencyMap_.rows; ++y)
		{
			imageP = saliencyMap_.ptr<uchar>(y);
			groundTruthP = groundTruth_.ptr<uchar>(y);
			for (x = 0; x < saliencyMap_.cols; ++x)
			{
				int pixSal = imageP[x];
				int pixGround = groundTruthP[x];

				if (pixSal > thresh)
				{
					sf++;
					if (pixGround != 0)
						hit++;
				}

				if (pixGround != 0)
					gf++;
			}
		}

		precision = (float)hit / (float)sf;
		recall = (float)hit / (float)gf;

		myfile << precision << " ";
		myfile << recall << "\n";

		hit = gf = sf = 0;
	}

	myfile.close();
}

void Comparison::computeROC()
{
	//file to store results
	ofstream myfile;

	//create a path for a file
	string rocPath = datasetUrl_ + rocDir + filename_ + ".txt";
	myfile.open(rocPath, std::ios_base::app);

	int tp(0), tn(0), fp(0), fn(0);
	float fpr, tpr;

	//loop through all the thresholds
	for (int thresh = 254; thresh >= 0; thresh--)
	{
		int y, x;
		uchar *imageP, *groundTruthP;
		for (y = 0; y < saliencyMap_.rows; ++y)
		{
			imageP = saliencyMap_.ptr<uchar>(y);
			groundTruthP = groundTruth_.ptr<uchar>(y);
			for (x = 0; x < saliencyMap_.cols; ++x)
			{
				int pixSal = imageP[x];
				int pixGround = groundTruthP[x];

				if (pixSal > thresh) //foreground
				{
					if (pixGround != 0) //fixated
						tp++;
					else
						fp++;
				}
				else { //background
					if (pixGround != 0) //fixated
						fn++;
					else
						tn++;
				}
			}
		}

		fpr = (float)fp / (float)(fp + tn);
		tpr = (float)tp / (float)(tp + fn);

		myfile << fpr << " ";
		myfile << tpr << "\n";

		tp = tn = fp = fn = 0;
	}

	myfile.close();
}

void Comparison::computeNSS() {
	//file to store results
	ofstream myfile;

	//create a path for a file
	string NSSPath = datasetUrl_ + NSSDir + filename_ + ".txt";
	myfile.open(NSSPath, std::ios_base::app);

	Mat salF;
	saliencyMap_.convertTo(salF, CV_32FC1);
	Mat mean, stddev;
	meanStdDev(salF, mean, stddev);
	salF = (salF - mean) / stddev;

	Mat nonZeroPixels;
	findNonZero(groundTruth_, nonZeroPixels);

	float accumSal = 0;
	int nonZeroCount = nonZeroPixels.total();
	for (int i = 0; i < nonZeroCount; i++) {
		Point pixelCoord = nonZeroPixels.at<Point>(i);
		accumSal += salF.at<float>(pixelCoord.y, pixelCoord.x);
	}

	myfile << accumSal / nonZeroCount << "\n";

	myfile.close();
}